import Vue from 'vue'
import firPortfolio23 from './view'

class Portfolio23 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {
      this.dataID = this.dataset.id;
    }

    connectedCallback () {
        this.initPortfolio23()
    }

    initPortfolio23 () {
        const { options } = this.props
        const config = {

        }

        this.app = new Vue({
          el: `[data-id="${this.dataID}"] .fir--vue`,
          components: {
            firPortfolio23
          }
        })

        // console.log("Init: Portfolio23")
    }

}

window.customElements.define('fir-portfolio-23', Portfolio23, { extends: 'section' })
