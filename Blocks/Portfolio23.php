<?php

namespace Fir\Pinecones\Portfolio23\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Portfolio23 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Portfolio';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Portfolio23.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['Portfolio23'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'post_type' => [
                'projects' => 'Projects',
                'news' => 'News',
                'services' => 'Services',
                'faqs' => 'FAQs'
            ]
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['post_type'] = ($data['content']['post_type']) ?: 'projects';
        $data['posts'] = get_posts(['post_type' => $data['post_type'], 'numberposts' => -1]);

        // $fields = get_post_meta($data['posts'][0]->ID);
        $data['custom'] = \Fir\Utils\Helpers::getMeta($data['posts']);

        $data['categories'] = [];
        foreach($data['posts'] as $x) {
            array_push($data['categories'], get_the_category($x->ID)[0]);
        }

        $data['cats'] = get_terms($data['posts']['slug']);
        array_splice($data['cats'], 0, 1);
        // array_splice($data['cats'], count($data['cats']) - 1, 1);

        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Portfolio23 = new FieldsBuilder('Portfolio');

        $Portfolio23
            ->addGroup('content', [
                'label' => 'Portfolio',
                'layout' => 'block'
            ])
                ->addSelect('post_type', [
                    'choices' => $this->defaults['content']['post_type']
                ])
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Portfolio23->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Portfolio23/style.css')->uri(), false, null);
    }
}
