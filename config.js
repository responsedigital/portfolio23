module.exports = {
    'name'  : 'Portfolio23',
    'camel' : 'Portfolio23',
    'slug'  : 'portfolio-23',
    'dob'   : 'Portfolio_23_1440',
    'desc'  : 'A slider with image and supporting text, next image slides shown on sides.',
}