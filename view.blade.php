<!-- Start Portfolio23 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A slider with image and supporting text, next image slides shown on sides. -->
@endif
<section class="{{ $block->classes }}" is="fir-portfolio-23" id="{{ $pinecone_id ?? '' }}" data-id="PostPreview{{ Fir\Utils\Helpers::getUniqueInt(1000, 9000) }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="portfolio-23 {{ $theme }} {{ $text_align }} {{ $flip }}">
      @include('Portfolio04.view', [
        'projects' => $posts,
        'custom' => $custom,
        'categories' => $categories,
        'cats' =>  $cats
      ])
      <div class="fir--vue">
        <fir-portfolio-23
          :projects="{{ json_encode($posts) }}"
          :custom="{{ json_encode($custom) }}"
          :categories="{{ json_encode($categories) }}"
          :cats="{{ json_encode($cats) }}"
        ></fir-portfolio-23>
      </div>
  </div>
</section>
<!-- End Portfolio23 -->
